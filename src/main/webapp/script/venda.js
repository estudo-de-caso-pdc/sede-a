angular.module('app')

// Create a controller with name vendasListControlle to bind to the grid section.
    .controller('vendasListController', function ($scope, $rootScope, vendaService) {
        // Initialize required information: sorting, the first page to show and the grid options.
        $scope.sortInfo = {fields: ['id'], directions: ['asc']};
        $scope.itemVendas = {currentPage: 1};

        $scope.vendas = [];

        vendaService.listar(null, function (data) {
            $scope.vendas = data;
        });


        $scope.gridOptions = {
            data: 'itemVendas.list',
            useExternalSorting: true,
            sortInfo: $scope.sortInfo,

            columnDefs: [
                {field: 'item.nome', displayName: 'Item'},
                {field: 'quantidade', displayName: 'Qtd'},
                {
                    field: '',
                    width: 30,
                    cellTemplate: '<span class="glyphicon glyphicon-remove remove" ng-click="deleteRow(row)"></span>'
                }
            ],

            multiSelect: false,
            selectedItemVendas: [],
            // Broadcasts an event when a row is selected, to signal the form that it needs to load the row data.
            afterSelectionChange: function (rowItemVenda) {
                if (rowItemVenda.selected) {
                    $rootScope.$broadcast('itemVendaSelected', $scope.gridOptions.selectedItemVendas[0].id);
                }
            }
        };

        // Refresh the grid, calling the appropriate rest method.
        $scope.refreshGrid = function () {
            if ($scope.selectedVenda) {
                $scope.itemVendas.list = $scope.selectedVenda.itemVendaList;
            }
        };

        // Broadcast an event when an element in the grid is deleted. No real deletion is perfomed at this point.
        $scope.deleteRow = function (row) {
            $rootScope.$broadcast('deleteItemVenda', row.entity.id);
        };

        // Watch the sortInfo variable. If changes are detected than we need to refresh the grid.
        // This also works for the first page access, since we assign the initial sorting in the initialize section.
        $scope.$watch('sortInfo', function () {
            $scope.itemVendas = {currentPage: 1};
            $scope.refreshGrid();
        }, true);

        // Do something when the grid is sorted.
        // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
        // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
        $scope.$on('ngGridEventSorted', function (event, sortInfo) {
            $scope.sortInfo = sortInfo;
        });

        // Picks the event broadcasted when a itemVenda is saved or deleted to refresh the grid elements with the most
        // updated information.
        $scope.$on('refreshGrid', function () {
            $scope.refreshGrid();
        });

        // Picks the event broadcasted when the form is cleared to also clear the grid selection.
        $scope.$on('clear', function () {
            $scope.gridOptions.selectAll(false);
        });
    })

    // Create a controller with name itemVendasFormController to bind to the form section.
    .controller('itemVendasFormController', function ($scope, $rootScope, vendaService) {
        // Clears the form. Either by clicking the 'Clear' button in the form, or when a successfull save is performed.
        $scope.clearForm = function () {
            $scope.itemVenda = null;
            // Resets the form validation state.
            $scope.itemVendaForm.$setPristine();
            // Broadcast the event to also clear the grid selection.
            $rootScope.$broadcast('clear');
        };

        // Calls the rest method to save a itemVenda.
        $scope.updateItemVenda = function () {
            vendaService.save($scope.itemVenda).$promise.then(
                function (data) {
                    // Broadcast the event to refresh the grid.
                    $rootScope.$broadcast('refreshGrid');
                    // Broadcast the event to display a save message.
                    $rootScope.$broadcast('saved');
                    $scope.clearForm();
                },
                function (data) {
                    // Broadcast the event for a server error.
                    $rootScope.$broadcast('error', data);
                });
        };

        // Picks up the event broadcasted when the itemVenda is selected from the grid and perform the itemVenda load by calling
        // the appropiate rest service.
        $scope.$on('itemVendaSelected', function (event, id) {
            $scope.itemVenda = vendaService.get({id: id});
        });

        // Picks us the event broadcasted when the venda is deleted from the grid and perform the actual venda delete by
        // calling the appropiate rest service.
        $scope.$on('deleteItemVenda', function (event, id) {
            vendaService.delete({id: id}).$promise.then(
                function () {
                    // Broadcast the event to refresh the grid.
                    $rootScope.$broadcast('refreshGrid');
                    // Broadcast the event to display a delete message.
                    $rootScope.$broadcast('deleted');
                    $scope.clearForm();
                },
                function () {
                    // Broadcast the event for a server error.
                    $rootScope.$broadcast('error');
                });
        });
    })

    // Create a controller with name itemVendasFormController to bind to the form section.
    .controller('vendasFormController', function ($scope, $rootScope, vendaService, itemService) {
        $scope.venda = {};
        $scope.items = [];

        itemService.listar(null, function (data) {
            $scope.items = data;
        });



        // Clears the form. Either by clicking the 'Clear' button in the form, or when a successfull save is performed.
        $scope.clearForm = function () {
            $scope.venda = null;
            // Resets the form validation state.
            $scope.vendaForm.$setPristine();
            // Broadcast the event to also clear the grid selection.
            $rootScope.$broadcast('clear');
        };

        // Calls the rest method to save a itemVenda.
        $scope.updateVenda = function () {
            var itemVendaList = [];

            angular.forEach($scope.venda.item, function (itm) {
                var item = {item: itm, quantidade: $scope.venda.quantidade};
                itemVendaList.push(item);
            });
            var vendas = {itemVendaList: itemVendaList};


            vendaService.save(vendas).$promise.then(
                function (data) {
                    // Broadcast the event to refresh the grid.
                    $rootScope.$broadcast('refreshGrid');
                    // Broadcast the event to display a save message.
                    $rootScope.$broadcast('saved');
                    $scope.clearForm();
                },
                function (data) {
                    // Broadcast the event for a server error.
                    $rootScope.$broadcast('error', data);
                });
        };
    })

    // Service that provides itemVendas operations
    .factory('vendaService', function ($resource) {
        return $resource('recursos/vendas/:id', {id: '@id'}, {
            query: {isArray: true},

            /**
             * Listagem resumida de Empresas informando se existe CCD válido
             */
            listar: {
                url: 'recursos/vendas',
                method: "GET",
                isArray: true
            }
        });


        //return $resource('recursos/vendas/:id');
    });
