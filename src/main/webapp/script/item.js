angular.module('app')

// Create a controller with name itemsListControlle to bind to the grid section.
    .controller('itemsListController', function ($scope, $rootScope, itemService) {
    // Initialize required information: sorting, the first page to show and the grid options.
    $scope.sortInfo = {fields: ['id'], directions: ['asc']};
    $scope.items = {currentPage: 1};

    $scope.gridOptions = {
        data: 'items.list',
        useExternalSorting: true,
        sortInfo: $scope.sortInfo,

        columnDefs: [
            { field: 'id', displayName: 'Id' },
            { field: 'nome', displayName: 'Nome' },
            { field: 'descricao', displayName: 'Descrição' },
            { field: 'estoque', displayName: 'Estoque' },
            { field: '', width: 30, cellTemplate: '<span class="glyphicon glyphicon-remove remove" ng-click="deleteRow(row)"></span>' }
        ],

        multiSelect: false,
        selectedItems: [],
        // Broadcasts an event when a row is selected, to signal the form that it needs to load the row data.
        afterSelectionChange: function (rowItem) {
            if (rowItem.selected) {
                $rootScope.$broadcast('itemSelected', $scope.gridOptions.selectedItems[0].id);
            }
        }
    };

    // Refresh the grid, calling the appropriate rest method.
    $scope.refreshGrid = function () {
        var listItemsArgs = {
            page: 0,
            sortFields: 'id',
            sortDirections: 'asc'
        };

        itemService.listar(listItemsArgs, function (data) {
            $scope.items.list = data;
        })
    };

    // Broadcast an event when an element in the grid is deleted. No real deletion is perfomed at this point.
    $scope.deleteRow = function (row) {
        $rootScope.$broadcast('deleteItem', row.entity.id);
    };

    // Watch the sortInfo variable. If changes are detected than we need to refresh the grid.
    // This also works for the first page access, since we assign the initial sorting in the initialize section.
    $scope.$watch('sortInfo', function () {
        $scope.items = {currentPage: 1};
        $scope.refreshGrid();
    }, true);

    // Do something when the grid is sorted.
    // The grid throws the ngGridEventSorted that gets picked up here and assigns the sortInfo to the scope.
    // This will allow to watch the sortInfo in the scope for changed and refresh the grid.
    $scope.$on('ngGridEventSorted', function (event, sortInfo) {
        $scope.sortInfo = sortInfo;
    });

    // Picks the event broadcasted when a item is saved or deleted to refresh the grid elements with the most
    // updated information.
    $scope.$on('refreshGrid', function () {
        $scope.refreshGrid();
    });

    // Picks the event broadcasted when the form is cleared to also clear the grid selection.
    $scope.$on('clear', function () {
        $scope.gridOptions.selectAll(false);
    });
})

// Create a controller with name itemsFormController to bind to the form section.
.controller('itemsFormController', function ($scope, $rootScope, itemService) {
    // Clears the form. Either by clicking the 'Clear' button in the form, or when a successfull save is performed.
    $scope.clearForm = function () {
        $scope.item = null;
        // Resets the form validation state.
        $scope.itemForm.$setPristine();
        // Broadcast the event to also clear the grid selection.
        $rootScope.$broadcast('clear');
    };

    // Calls the rest method to save a item.
    $scope.updateItem = function () {
        itemService.save($scope.item).$promise.then(
            function (data) {
                // Broadcast the event to refresh the grid.
                $rootScope.$broadcast('refreshGrid');
                // Broadcast the event to display a save message.
                $rootScope.$broadcast('itemSaved');
                $scope.clearForm();
            },
            function (data) {
                // Broadcast the event for a server error.
                $rootScope.$broadcast('error', data);
            });
    };

    // Picks up the event broadcasted when the item is selected from the grid and perform the item load by calling
    // the appropiate rest service.
    $scope.$on('itemSelected', function (event, id) {
        $scope.item = itemService.get({id: id});
    });

    // Picks us the event broadcasted when the item is deleted from the grid and perform the actual item delete by
    // calling the appropiate rest service.
    $scope.$on('deleteItem', function (event, id) {
        itemService.delete({id: id}).$promise.then(
            function () {
                // Broadcast the event to refresh the grid.
                $rootScope.$broadcast('refreshGrid');
                // Broadcast the event to display a delete message.
                $rootScope.$broadcast('itemDeleted');
                $scope.clearForm();
            },
            function () {
                // Broadcast the event for a server error.
                $rootScope.$broadcast('error');
            });
    });
})

// Service that provides items operations
.factory('itemService', function ($resource) {
    return $resource('recursos/items/:id', {id: '@id'}, {
        query: {isArray: true},

        /**
         * Listagem resumida de Empresas informando se existe CCD válido
         */
        listar: {
            url:'recursos/items',
            method:"GET",
            isArray: true
        }
    });


    //return $resource('recursos/items/:id');
});
