angular.module('app', ['ngResource', 'ngGrid', 'ui.bootstrap'])

// Create a controller with name alertMessagesController to bind to the feedback messages section.
    .controller('alertMessagesController', function ($scope) {
        // Picks up the event to display a saved message.
        $scope.$on('saved', function () {
            $scope.alerts = [
                {type: 'success', msg: 'Record saved successfully!'}
            ];
        });

        // Picks up the event to display a deleted message.
        $scope.$on('deleted', function () {
            $scope.alerts = [
                {type: 'success', msg: 'Record deleted successfully!'}
            ];
        });

        // Picks up the event to display a server error message.
        $scope.$on('error', function (data, info) {
            $scope.alerts = [
                {type: 'danger', msg: 'There was a problem in the server: ' + info.statusText}
            ];
        });

        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
    });