package info.peixedevidro.rest;

import info.peixedevidro.negocio.dto.ItemDto;
import info.peixedevidro.negocio.item.IServicoItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Stateless
@Path("items")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ItemRecurso {

    @EJB(mappedName = IServicoItem.JNDI_NAME)
    private IServicoItem servicoItem;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SuppressWarnings("unchecked")
    public List<ItemDto> listar() {
        return servicoItem.listar();
    }

    @GET
    @Path("{id}")
    public ItemDto getItem(@PathParam("id") Long id) {
        return servicoItem.obter(id);
    }

    @Transactional
    @POST
    public ItemDto salvarItem(ItemDto itemDto) {
        if (itemDto.getId() == null) {
            return servicoItem.incluir(itemDto);
        } else {
            return servicoItem.atualizar(itemDto);
        }
    }

    @DELETE
    @Path("{id}")
    public void apagarItem(@PathParam("id") Long id) {
        servicoItem.excluir(id);
    }
}
